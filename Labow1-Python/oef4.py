import numpy as np

answer = "y"

M = []

while answer == "y":
    row = []

    for i in range(4):
        row.append(float(input("Num %d? " % (i + 1))))

    M.append(row)

    answer = input("Continue (y|n)? ")

print(np.array(M))
