#!/usr/bin/python

from Dataset import Dataset
from Model import Model

def main():
    data = Dataset("./train.csv")

    model = Model(data.sizeX)

    model.fit(data)

    print(model.W)
    print(model.B)

    err = data.error(model.calculate(data))
    print(err)

if __name__ == "__main__":
    main()
