import numpy as np
from linearmodel import LinearModel

class LinearRegression:
    def __init__(self, dataset, learningRate = 0.1, epsilon = 0.1):
        self.dataset = dataset
        self.model = LinearModel()
        self.learningRate = learningRate
        self.epsilon = epsilon

    def execute(self):

        b = 0
        m = 0

        while np.abs(self.model.slope - m) > self.epsilon or\
              np.abs(self.model.intercept - b) > self.epsilon:

            m = self.model.slope
            b = self.model.intercept

            self.model.slope = m - self.learningRate * self.model.dcostm(self.dataset)
            self.model.intercept = b - self.learningRate * self.model.dcostb(self.dataset)

        return self.model
