import numpy as np
import numpy.random as random

class Model:
    def __init__(self, size, learningRate = 0.001, epsilon = 1e-10):

        self.size = size

        self.learningRate = learningRate
        self.epsilon = epsilon

        self.W = self.random(size)
        self.B = self.random(1)

    def random(self, size):
        return np.asmatrix( (random.random(size) * 0.001 ))

    def zeros(self, size):
        return np.asmatrix(np.zeros(size))

    def sigmoid(self, expr):
        return 1 / 1 + np.exp(-1 * expr)

    def sigmoidModel(self, dataset):
        return self.sigmoid(self.calculate(dataset))

    def calculate(self, dataset):
        return self.W * dataset.getX() + self.B

    def deltaW(self, dataset):
        return self.W * self.deltaB(dataset)

    def deltaB(self, dataset):
        return (1 / dataset.datapoints) * np.sum( np.transpose(self.sigmoidModel(dataset)) - dataset.getY() )

    def fit(self, dataset):
        W = self.zeros(self.size)
        B = self.zeros(self.size)

        while np.any( np.abs(self.W - W) > self.epsilon ) or \
                np.any( np.abs(self.B - B) > self.epsilon ):

            W = self.W
            B = self.B

            print(W)

            tempW = W - self.learningRate * self.deltaW(dataset)
            tempB = B - self.learningRate * self.deltaB(dataset)

            self.W = tempW
            self.B = tempB


