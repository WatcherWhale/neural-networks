import numpy as np

class Dataset:
    def __init__(self, file):
        self.file = file
        self.data = np.genfromtxt(self.file, delimiter=';')

        self.sizeX = self.data.shape[1] - 1
        self.datapoints = self.data.shape[0]

    def getX(self):
        return np.transpose(np.asmatrix(self.data[:, 1:self.sizeX + 1]))

    def getY(self):
        return np.transpose(np.asmatrix(self.data[:, 0 ]))

    def error(self, y_hat):
        return np.sum( ( y_hat - self.getY() ) ** 2 ) / self.datapoints
