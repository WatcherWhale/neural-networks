import numpy as np
import matplotlib.pyplot as plt

from linearregression import LinearRegression

train_data = np.genfromtxt('train_fictief.csv', delimiter=';')

plt.scatter(train_data[:,0], train_data[:,1], label = "Dataset")

t = np.arange(0, 8, 0.2)
plt.plot(t, 0.5 * t + 4, "k--", label = "Guess")


reg = LinearRegression(train_data, learningRate= 0.001, epsilon= 10 ** -20)

model = reg.execute()
print(model)
print("Cost %.2f" % model.cost(train_data))

plt.plot(t, model.model(t), "b-", label = "Computed model")

plt.legend()
plt.show()
