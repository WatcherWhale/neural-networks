import numpy as np
import tensorflow as tf
from tensorflow import keras

import random

shape = (200, 200)
shape3 = (shape[0], shape[1], 3)
batch_size = 32

input = keras.layers.Input(shape = shape3, batch_size = batch_size)

output = keras.layers.Conv2D(filters=64, kernel_size=7)(input)
output = keras.layers.Activation("relu")(output)
output = keras.layers.BatchNormalization()(output)
output = keras.layers.MaxPool2D()(output)

output = keras.layers.Conv2D(filters=16, kernel_size=7)(output)
output = keras.layers.Activation("relu")(output)
output = keras.layers.BatchNormalization()(output)
output = keras.layers.MaxPool2D()(output)

output = keras.layers.Conv2D(filters=4, kernel_size=7)(output)
output = keras.layers.Activation("relu")(output)
output = keras.layers.BatchNormalization()(output)
output = keras.layers.MaxPool2D()(output)

output = keras.layers.Flatten()(output)
output = keras.layers.BatchNormalization()(output)
output = keras.layers.Dense(1, activation="sigmoid")(output)

model = keras.models.Model(inputs=input, outputs=output)

model.compile(optimizer="adam", loss="mae", metrics=["acc"])

model.summary()

seed = 1337
train_ds = tf.keras.preprocessing.image_dataset_from_directory('data/train/', subset="training", validation_split=0.2, image_size = shape, crop_to_aspect_ratio = False, seed = seed, batch_size = batch_size)
valid_ds = tf.keras.preprocessing.image_dataset_from_directory('data/train/', subset="validation", validation_split=0.2, image_size= shape, crop_to_aspect_ratio = False, seed = seed, batch_size = batch_size)

while True:
    model.fit(train_ds, batch_size=32, epochs = 1, validation_data= valid_ds)
    model.save("model.h5")
