import numpy as np

class LinearModel:
    def __init__(self, slopeRange = [0, 1], interceptRange = [0, 1]):
        self.slope = ( np.random.random() * ( slopeRange[1] - slopeRange[0] ) ) + slopeRange[0]
        self.intercept = ( np.random.random() * ( interceptRange[1] - interceptRange[0] ) ) + interceptRange[0]

    def __str__(self):
        return "%.2f" % self.slope + "X + %.2f" % self.intercept

    def model(self, x):
        return self.slope * x + self.intercept

    def cost(self, dataset):
        n = np.size(dataset, 1)
        return (1 / n) * np.sum( ( dataset[:, 1] - self.model(dataset[:, 0]) ) ** 2 )

    def dcostm(self, dataset):
        m = np.size(dataset, 1)

        w = self.slope
        b = self.intercept

        xi = dataset[:, 0]
        yi = dataset[:, 1]


        return (-2 / m) * np.sum( xi * ( yi - (w * xi + b) ) )


    def dcostb(self, dataset):
        m = np.size(dataset, 1)

        w = self.slope
        b = self.intercept

        xi = dataset[:, 0]
        yi = dataset[:, 1]

        return (-2 / m) * np.sum( yi - (w * xi + b) )
