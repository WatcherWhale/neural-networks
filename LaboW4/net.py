import numpy as np
import tensorflow as tf
from tensorflow import keras

import random

shape = (200, 200)
shape3 = (shape[0], shape[1], 3)
batch_size = 32

model = keras.Sequential()
model.add(keras.Input(shape = shape3, batch_size = batch_size))

# Rescaling & Flattening
model.add(keras.layers.Rescaling(1./255))
model.add(keras.layers.Flatten())

model.add(keras.layers.Dense(1024))
model.add(keras.layers.Dense(128))
model.add(keras.layers.Dense(32))

# Output
model.add(keras.layers.Dense(1, activation="sigmoid"))


model.compile(optimizer="Ftrl", loss="mse", metrics=["acc"])

model.summary()

seed = 1337
train_ds = tf.keras.preprocessing.image_dataset_from_directory('data/train/', subset="training", validation_split=0.2, image_size = shape, crop_to_aspect_ratio = False, seed = seed, batch_size = batch_size)
valid_ds = tf.keras.preprocessing.image_dataset_from_directory('data/train/', subset="validation", validation_split=0.2, image_size= shape, crop_to_aspect_ratio = False, seed = seed, batch_size = batch_size)

while True:
    model.fit(train_ds, batch_size=32, epochs = 5)
    result = model.evaluate(valid_ds)
    print(result)
    model.save("model.h5")


