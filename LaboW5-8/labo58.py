from pprint import pprint

import numpy as np

import sklearn
import sklearn.datasets

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split

iris = sklearn.datasets.load_iris()

x,y = iris.data, iris.target


model_parameters = {
    'n_estimators': [50, 100, 200],
    'max_features': ['auto', 'sqrt', 'log2'],
    'min_samples_split': np.random.uniform(0, 1, 10)
}

classifier = RandomForestClassifier()
gscv = RandomizedSearchCV(classifier, model_parameters)

model = gscv.fit(x, y)
pprint(model.best_estimator_.get_params())
