import math
import numpy
import numpy as np
import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error

numpy.random.seed(1337)

dataframe = pandas.read_csv("./airline.csv", usecols=[1], engine="python")
dataset = dataframe.values.astype('float32')

scaler = MinMaxScaler(feature_range=(0, 1))
dataset = scaler.fit_transform(dataset)

print(dataset.shape)

train = int(len(dataset) * 0.7)
test = len(dataset) - train

train, test = dataset[0:train, :], dataset[train:len(dataset), :]

def create_matrix(dataset, look_back = 1):
    x, y = [], []
    for i in range(len(dataset) - look_back - 1):
        a = dataset[i:(i+look_back), 0]
        x.append(a)
        y.append(dataset[i + look_back, 0])
    return numpy.array(x), numpy.array(y)

look_back = 1

train_x, train_y = create_matrix(train, look_back)
test_x, test_y = create_matrix(test, look_back)

train_x = numpy.reshape(train_x, (train_x.shape[0], 1, train_x.shape[1]))
test_x = numpy.reshape(test_x, (test_x.shape[0], 1, test_x.shape[1]))

model = Sequential()
model.add(LSTM(4, input_shape=(1, look_back)))
model.add(Dense(1))
model.compile(loss='mse', optimizer='adam')


model.fit(train_x, train_y, epochs=200, batch_size=1)

predict_y = model.predict(test_x)
predict_y = scaler.inverse_transform(predict_y)
test_y = scaler.inverse_transform([test_y])

testScore = math.sqrt(mean_squared_error(test_y[0], predict_y[:,0]))

print("RMSE %f" % testScore)


predictPlot = np.empty_like(dataset)
predictPlot[:, :] = np.nan
predictPlot[len(dataset) - len(predict_y):] = predict_y


plt.plot(scaler.inverse_transform(dataset))
plt.plot(predictPlot, linestyle="dashed")
plt.show()































