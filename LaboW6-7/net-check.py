import numpy as np
import tensorflow as tf
from tensorflow import keras
import datetime

shape = (200, 200)
shape3 = (shape[0], shape[1], 3)
batch_size = 32

model = tf.keras.models.load_model("model.h5")
model.summary()

seed = 1337
valid_ds = tf.keras.preprocessing.image_dataset_from_directory('data/train/', subset="validation", validation_split=0.2, image_size= shape, crop_to_aspect_ratio = False, seed = seed, batch_size = batch_size)

result = model.evaluate(valid_ds)
