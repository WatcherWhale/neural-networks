import numpy as np
import matplotlib.pyplot as plt

t = np.arange(0, 5, 0.2)

plt.plot(t, np.cos(t), "mo" )
plt.show()
